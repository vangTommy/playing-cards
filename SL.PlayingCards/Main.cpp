#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE= 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14,
};
enum Suit 
{
	CLUB = 1,
	DIAMOND = 2,
	HEART = 3,
	SPADE = 4,
};
struct Card 
{
	int rank;
	int suit;
};

void PrintCard(Card card1)
{
	switch (card1.rank)
	{
	case TWO: if (card1.rank == 2) { cout << "The TWO of "; };
	case THREE: if (card1.rank == 3) { cout << "The THREE of "; };
	case FOUR: if (card1.rank == 4) { cout << "The FOUR of "; };
	case FIVE: if (card1.rank == 5) { cout << "The FIVE of "; };
	case SIX: if (card1.rank == 6) { cout << "The SIX of "; };
	case SEVEN: if (card1.rank == 7) { cout << "The SEVEN of "; };
	case EIGHT: if (card1.rank == 8) { cout << "The EIGHT of "; };
	case NINE: if (card1.rank == 9) { cout << "The NINE of "; };
	case TEN: if (card1.rank == 10) { cout << "The TEN of "; };
	case JACK: if (card1.rank == 11) { cout << "The JACK of "; };
	case QUEEN: if (card1.rank == 12) { cout << "The QUEEN of "; };
	case KING: if (card1.rank == 13) { cout << "The KING of "; };
	case ACE: if (card1.rank == 14) { cout << "The ACE of "; };
	default:
		break;
	}
	switch (card1.suit)
	{
	case CLUB: if (card1.suit == 0) { cout << "CLUB"; };
	case DIAMOND: if (card1.suit == 1) { cout << "DIAMOND"; };
	case HEART: if (card1.suit == 2) { cout << "HEART"; };
	case SPADE: if (card1.suit == 3) { cout << "SPADE"; };
	default:
		break;
	}
}

void HighCard(Card card1, Card card2)
{
	if (card1.rank == card2.rank)
	{
		if (card1.suit > card2.suit)
		{
			PrintCard(card1);
			cout << " Is greater than ";
			PrintCard(card2);
		}
		else if (card1.suit == card2.suit)
		{
			PrintCard(card1);
			cout << " Is Equal to ";
			PrintCard(card2);
		}
		else
		{
			PrintCard(card1);
			cout << " Is less than ";
			PrintCard(card2);
		}
	}
	else
	{
		if (card1.rank > card2.rank)
		{
			PrintCard(card1);
			cout << " Is greater than ";
			PrintCard(card2);
		}
		else
		{
			PrintCard(card1);
			cout << " Is less than ";
			PrintCard(card2);
		}
	}
}

int main() 
{
	Card card1;
	Card card2;

	card1.rank;//Holds the first cards rank
	card1.suit;//Holds the first cards suit

	card2.rank;//Holds the second cards rank
	card2.suit;//Holds the second cards suit

	//Ask for card1
	cout << "Enter a Number from 2 to 14 and Enter either 0,1,2,3 For the First Card\n"; // enter a value from 2 to 14, and that will give a rank
	cin >> card1.rank >> card1.suit; //card1.ranks value equals the input of this.
	PrintCard(card1);
	cout << "\n";

	//Ask for card2
	cout << "\nEnter a Number from 2 to 14 and Enter either 0,1,2,3 For the Second Card\n"; // Enter a value from 1 to 4 to get the suit
	cin >> card2.rank >> card2.suit;
	PrintCard(card2);
	cout << "\n";

	HighCard(card1, card2);


	_getch();
	return 0;
};

